//import 'dart:indexed_db';

import 'dart:async';
import 'dart:typed_data';

import 'package:requests/requests.dart';
import 'dart:io';
import 'dart:convert';

String url = 'https://api.github.com/users/Flutterando';
// url camara
//String url = 'https://dadosabertos.camara.leg.br/arquivos/proposicoes/json/proposicoes-2023.json';

// icones do loop
List<dynamic> chars = ["-", "\\", "|", "/"];
final os_sep = Platform.pathSeparator;
final fileLocalJson = "info${os_sep}file.json";

Future<String> get_online_json(String url) async {
  // Faz o request em uma url e retorna o conteúdo.
  var req = await Requests.get(url); 
  return Future.value(req.content());

}

class SaveJson{

  String url;
  String filename;
  String? json_value;
  //Map<String, dynamic>? onlineValue;

  SaveJson({required this.url, required this.filename, this.json_value});

  Future<String> getOnlineJson(String url) async {
    var req = await Requests.get(url);
    return Future.value(req.content());
  }

  Future<Map<String, dynamic>> jsonToMapFromUrl(String url) async {
    // Recebe um url, faz o request do json, convert o Json para Map e retorna o Map.
    Future<String> onlineJsonFuture = this.getOnlineJson(url);
    String online = await onlineJsonFuture;
    return json.decode(online) as Map<String, dynamic>;
    
  }

  Map<String, dynamic> jsonToMap(String dataJson){
    var _map = json.decode(dataJson) as Map<String, dynamic>;
    return _map;
  }

  void tofile() async{
    // Salva o conteúdo de this.url em um arquivo this.filename.
    String jsonText;
    Future<String> jsonFuture = get_online_json(this.url);
    jsonText = await jsonFuture;
    List<String> jsonList = jsonText.split(',');
    
    var f_obj = File(this.filename);
    if(await f_obj.existsSync()){
      print("Limpando o arquivo ${this.filename}");
      f_obj.writeAsStringSync("", mode: FileMode.write);
    }

    jsonList.forEach((element) => f_obj.writeAsStringSync("$element\n", mode: FileMode.append));    
  }


}


void aguardar() {
  int count = 0;
  int local_time = 0;
  String value;
  
  while(true){

    if(count > 3){
      count = 0;
    }

    value = chars.elementAt(count);
    stdout.write("Aguardando -> [$value] $local_time s.\r");
    count ++;
    local_time ++;
    sleep(Duration (seconds: 1));

    if(local_time > 9){
      print("Aguardando -> [$value] $local_time s. \r");
      break;
    }

  }


}

void run() async {

  //Future<String> dataFuture = get_online_json(url); 
  //String data;
  // data = await dataFuture;

  SaveJson save_json = new SaveJson(url: url, filename: fileLocalJson);
  //Future<String> onlineFuture = save_json.getOnlineJson(url);
  //String online = await onlineFuture;

  var futureRequest = save_json.getOnlineJson(url);
  aguardar();
  String string_req = await futureRequest;

  String _lines = "===============================================================";

  save_json.jsonToMap(string_req).forEach((key, value) => print("${_lines}\n [KEY] ${key} -> [VALOR] ${value}"));

}