/*


KEYS

id
uri
siglaTipo
numero
ano
codTipo
descricaoTipo
ementa
ementaDetalhada
keywords
dataApresentacao
uriOrgaoNumerador
uriPropAnterior
uriPropPrincipal
uriPropPosterior
urlInteiroTeor
ultimoStatus

*/

import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:libjson/utils.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

String url = 'https://dadosabertos.camara.leg.br/arquivos/proposicoes/json/proposicoes-2024.json';
String urlZip = 'https://www.camara.leg.br/cotas/Ano-2024.json.zip';
String outputFile = joinPath(getUserDownloads(), 'camara.zip');
String outputJsonFile = joinPath(getUserDownloads(), 'proposições.json');

// url camara
//String url = 'https://dadosabertos.camara.leg.br/arquivos/proposicoes/json/proposicoes-2023.json';

// icones do loop
List<dynamic> chars = ["-", "\\", "|", "/"];
final fileLocalJson = "info${Platform.pathSeparator}file.json";

//========================================================================//
// Request de um URL qualquer.
//========================================================================//
Future<Response> getRequest(String url) async {
  // Recebe um URL e retorna um objeto RESPONSE.
  printInfo('Request: ${url}');
  var u = Uri.parse(url);
  var response = await http.get(u);
  var r = response as http.Response;
  return r;
}

//========================================================================//
// Ao instânciar essa classe você pode obter um Map<> de um conteúdo JSON
// a fonte pode ser um ARQUIVO local ou um URL.
//========================================================================//
class JsonToMap {
  Future<Map<String, dynamic>> fromUrl(String url) async {
    // Recebe um url de arquivo JSON, baixa o conteúdo e retorna em forma de mapa.
    Future<Response> response = getRequest(url);
    Response r = await response;
    Map<String, dynamic> m = jsonDecode(utf8.decode(r.bodyBytes));
    return m;
  }

  Map<String, dynamic> fromFileName(String filename) {
    // Recebe o caminho completo de um arquivo JSON no disco e retorna o conteúdo
    // em forma de mapa.
    File f = File(filename);
    if (f.existsSync() == false) {
      printLine();
      printErro('o arquivo não existe ${filename}');
    }
    printInfo('Lendo o arquivo: ${filename}');
    String content = f.readAsStringSync();
    Map<String, dynamic> m = jsonDecode(content);
    return m;
  }
}

void parse(){
  Map<String, dynamic> fj = JsonToMap().fromFileName(outputJsonFile);
  List<dynamic> listValues = fj['dados'];
  Map<String, dynamic> current;
  int max= listValues.length;
  
  for(int i=0; i<max; i++){
    current = listValues[i];
    if(current.containsKey('ementa')){
      printLine();
      print(current['ementa']);
    }
  }

}

void run() async {
  
  parse();
  

  print('OK');
  return;
}
